package maestro.my.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artyom on 9/4/2015.
 */
public class User implements Parcelable {

    private String mId;
    private String mEmail;
    private String mFirstName;
    private String mLastName;
    private String mCountryIso;
    private long mBirthDate;
    private long mGender;

    public User() {
    }

    public User(Parcel source) {
        mId = source.readString();
        mEmail = source.readString();
        mGender = source.readInt();
        mFirstName = source.readString();
        mLastName = source.readString();
        mBirthDate = source.readLong();
        mCountryIso = source.readString();
    }

    public User setId(String id) {
        mId = id;
        return this;
    }

    public User setEmail(String email) {
        mEmail = email;
        return this;
    }

    public User setGender(long gender) {
        mGender = gender;
        return this;
    }

    public User setFirstName(String firstName) {
        mFirstName = firstName;
        return this;
    }

    public User setLastName(String lastName) {
        mLastName = lastName;
        return this;
    }

    public User setBirthDate(long birthDate) {
        mBirthDate = birthDate;
        return this;
    }

    public User setCountryIso(String countryIso) {
        mCountryIso = countryIso;
        return this;
    }

    public String getId() {
        return mId;
    }

    public String getEmail() {
        return mEmail;
    }

    public long getGender() {
        return mGender;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public long getBirthDate() {
        return mBirthDate;
    }

    public String getCountryIso() {
        return mCountryIso;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mEmail);
        dest.writeLong(mGender);
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeLong(mBirthDate);
        dest.writeString(mCountryIso);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
