package maestro.my.api;

import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.firebase.client.FirebaseError;
import maestro.my.api.model.User;
import maestro.my.api.request.BaseRequest;
import maestro.my.api.ui.FloatingEditText;
import maestro.svg.SVGInstance;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Artyom on 9/4/2015.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<BaseRequest> {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private static final String PARAM_CREATE = "create";
    private static final String PARAM_IN_PROGRESS = "in_progress";
    private static final String PARAM_IS_LOGGED_IN = "is_logged_in";
    private static final String PARAM_STORED_COUNTRY_POSITION = "position";
    private static final String PARAM_STORED_DATE = "date";
    private static final String PARAM_STORED_GENDER = "gender";

    private static final int IMAGE_LOADER = "image_loader".hashCode();

    private TextView mTitle;
    private TextView mErrorEmail;
    private TextView mErrorPassword;
    private TextView mPasswordRemind;
    private TextView mCurrentDate;

    private FloatingEditText mEmailEdit;
    private FloatingEditText mPasswordEdit;
    private FloatingEditText mFirstNameEdit;
    private FloatingEditText mLastNameEdit;
    private Spinner mCountrySpinner;
    private RadioGroup mGenderGroup;

    private ImageView mSocialFacebook;
    private ImageView mSocialGoogle;
    private ImageView mSocialTwitter;
    private ImageView btnFillConfirm;
    private ImageView mImage;

    private Button btnCreate;
    private Button btnSignIn;

    private View mProgressBar;
    private View mOverflowView;
    private View mFillInfoParent;
    private View mOverflowShadow;

    private CharSequence apiName = "alpha";
    private CharSequence apiFont = "fonts/BAUHS93.TTF";
    private CharSequence apiImage = "https://pp.vk.me/c623721/v623721990/41e1c/60sYsOsnaBM.jpg";

    private Calendar mCalendar;
    private ColorMatrixColorFilter mImageFilter;
    private CleanTextWatcher mTextWatcher = new CleanTextWatcher();
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd MMMM yyyy");

    {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        mImageFilter = new ColorMatrixColorFilter(matrix);
    }

    private boolean isOperationInProgress;
    private boolean isLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SVGInstance.initialize(this);
        MyApi.getInstance().initialize(getApplication(), null);

        setContentView(R.layout.main);

        mTitle = (TextView) findViewById(R.id.title);
        mErrorEmail = (TextView) findViewById(R.id.email_error);
        mErrorPassword = (TextView) findViewById(R.id.password_error);
        mPasswordRemind = (TextView) findViewById(R.id.password_remind);
        mEmailEdit = (FloatingEditText) findViewById(R.id.email_edit_text);
        mPasswordEdit = (FloatingEditText) findViewById(R.id.password_edit_text);

        mImage = (ImageView) findViewById(R.id.image);
        mSocialFacebook = (ImageView) findViewById(R.id.login_social_facebook);
        mSocialGoogle = (ImageView) findViewById(R.id.login_social_google);
        mSocialTwitter = (ImageView) findViewById(R.id.login_social_twitter);

        btnCreate = (Button) findViewById(R.id.btn_create_account);
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);

        mProgressBar = findViewById(R.id.progress_parent);
        mOverflowView = findViewById(R.id.overflow_view);
        mFillInfoParent = findViewById(R.id.screen_fill_info);
        mOverflowShadow = findViewById(R.id.shadow_overflow);

        SVGInstance.applySVG(mSocialFacebook, R.raw.facebook42);
        SVGInstance.applySVG(mSocialGoogle, R.raw.google42);
        SVGInstance.applySVG(mSocialTwitter, R.raw.twitter42);

        btnCreate.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);

        mTextWatcher.addTextView(mErrorEmail);
        mTextWatcher.addTextView(mErrorPassword);
        mEmailEdit.addTextChangedListener(mTextWatcher);
        mPasswordEdit.addTextChangedListener(mTextWatcher);

        TypedValue value = new TypedValue();
        getTheme().resolveAttribute(R.attr.my_api_name, value, true);
        apiName = !TextUtils.isEmpty(value.coerceToString()) ? value.coerceToString() : apiName;
        getTheme().resolveAttribute(R.attr.my_api_font, value, true);
        apiFont = !TextUtils.isEmpty(value.coerceToString()) ? value.coerceToString() : apiFont;
        getTheme().resolveAttribute(R.attr.my_api_image, value, true);
        apiImage = !TextUtils.isEmpty(value.coerceToString()) ? value.coerceToString() : apiImage;

        mImage.setColorFilter(mImageFilter);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(), apiFont.toString()));
        mTitle.setText(apiName);
        mPasswordRemind.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Italic.ttf"));

        getSupportLoaderManager().initLoader(IMAGE_LOADER, null, new LoaderManager.LoaderCallbacks<Bitmap>() {
            @Override
            public Loader<Bitmap> onCreateLoader(int i, Bundle bundle) {
                return new ImageLoader(getBaseContext(), apiImage);
            }

            @Override
            public void onLoadFinished(Loader<Bitmap> loader, Bitmap o) {
                mImage.setImageBitmap(o);
                mOverflowView.animate().alpha(.66f).setDuration(450).start();
            }

            @Override
            public void onLoaderReset(Loader<Bitmap> loader) {

            }
        });

        getSupportLoaderManager().initLoader(IMAGE_LOADER + 1, null, new LoaderManager.LoaderCallbacks<Bitmap>() {
            @Override
            public Loader<Bitmap> onCreateLoader(int i, Bundle bundle) {
                return new ImageLoader(getBaseContext(), "https://pp.vk.me/c625723/v625723990/49326/cF9wCLMk6_E.jpg");
            }

            @Override
            public void onLoadFinished(Loader<Bitmap> loader, Bitmap bitmap) {
                BitmapDrawable drawable = new BitmapDrawable(bitmap);
                drawable.setColorFilter(mImageFilter);
                drawable.setAlpha((int) (255 * .3f));
                LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{new ColorDrawable(Color.WHITE), drawable});
                mFillInfoParent.setBackgroundDrawable(layerDrawable);
            }

            @Override
            public void onLoaderReset(Loader<Bitmap> loader) {

            }
        });

        if (true) {
            mEmailEdit.setText("psevdomaestro@gmail.com");
            mPasswordEdit.setText("1qw31qw3");
        }

    }

    private void prepareFillScreen(Bundle savedState) {
        mFirstNameEdit = (FloatingEditText) findViewById(R.id.first_name_edit);
        mLastNameEdit = (FloatingEditText) findViewById(R.id.last_name_edit);
        mCountrySpinner = (Spinner) findViewById(R.id.country_spinner);
        mGenderGroup = (RadioGroup) findViewById(R.id.gender_group);
        mCurrentDate = (TextView) findViewById(R.id.date_current);
        btnFillConfirm = (ImageView) findViewById(R.id.btn_fill_confirm);

        TypedValue value = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, value, true);

        Drawable drawable = SVGInstance.getDrawable(R.raw.el_primary_action_button, Color.parseColor("#444444"), value.data);
        btnFillConfirm.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        btnFillConfirm.setBackgroundDrawable(drawable);
        btnFillConfirm.setImageDrawable(null);

        btnFillConfirm.setImageDrawable(SVGInstance.getDrawable(R.raw.ic_check, Color.WHITE));

        final User user = MyApi.getInstance().getUser();
        if (savedState == null) {
            mFirstNameEdit.setText(user.getFirstName());
            mLastNameEdit.setText(user.getLastName());
        }

        LocaleAdapter mAdapter = new LocaleAdapter(getBaseContext());
        mCountrySpinner.setAdapter(mAdapter);
        mCountrySpinner.setSelection(savedState != null && savedState.containsKey(PARAM_STORED_COUNTRY_POSITION)
                ? savedState.getInt(PARAM_STORED_COUNTRY_POSITION)
                : mAdapter.getDefault(user.getCountryIso()));

        mGenderGroup.check(savedState != null && savedState.containsKey(PARAM_STORED_GENDER)
                ? savedState.getInt(PARAM_STORED_GENDER)
                : user.getGender() == 0 ? R.id.gender_male : user.getGender() == 1
                ? R.id.gender_female : R.id.gender_other);

        mCalendar = Calendar.getInstance();
        if (savedState != null && savedState.containsKey(PARAM_STORED_DATE)) {
            mCalendar.setTimeInMillis(savedState.getLong(PARAM_STORED_DATE));
        } else if (user.getBirthDate() != 0) {
            mCalendar.setTimeInMillis(user.getBirthDate());
        }

        mCurrentDate.setText(mDateFormat.format(mCalendar.getTime()));

        findViewById(R.id.date_parent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(LoginActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mCalendar.set(year, monthOfYear, dayOfMonth);
                        mCurrentDate.setText(mDateFormat.format(mCalendar.getTime()));
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        btnFillConfirm.setOnClickListener(this);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PARAM_IN_PROGRESS, isOperationInProgress);
        outState.putBoolean(PARAM_IS_LOGGED_IN, isLoggedIn);
        if (isLoggedIn) {
            outState.putInt(PARAM_STORED_COUNTRY_POSITION, mCountrySpinner.getSelectedItemPosition());
            outState.putLong(PARAM_STORED_DATE, mCalendar.getTimeInMillis());
            outState.putInt(PARAM_STORED_GENDER, mGenderGroup.getCheckedRadioButtonId());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isOperationInProgress = savedInstanceState.getBoolean(PARAM_IN_PROGRESS);
        setProgressVisibility(isOperationInProgress);
        isLoggedIn = savedInstanceState.getBoolean(PARAM_IS_LOGGED_IN);
        if (isLoggedIn){
            mFillInfoParent.setVisibility(View.VISIBLE);
            prepareFillScreen(savedInstanceState);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_create_account) {
            processAuthorization(true);
        } else if (v.getId() == R.id.btn_sign_in) {
            processAuthorization(false);
        } else if (v.getId() == R.id.btn_fill_confirm) {
            User user = MyApi.getInstance().getUser();
            user.setFirstName(mFirstNameEdit.getText().toString());
            user.setLastName(mLastNameEdit.getText().toString());
            user.setBirthDate(mCalendar.getTimeInMillis());
            final int checkedGender = mGenderGroup.getCheckedRadioButtonId();
            user.setGender(checkedGender == R.id.gender_male ? 0 : checkedGender == R.id.gender_female ? 1 : 2);
            user.setCountryIso(((Locale) mCountrySpinner.getAdapter().getItem(mCountrySpinner.getSelectedItemPosition())).getCountry());
            MyApi.getInstance().updateUserData(user);
            finish();
        }
    }

    private void processAuthorization(boolean create) {
        if (!isOperationInProgress) {
            isOperationInProgress = true;
            Bundle bundle = new Bundle(1);
            bundle.putBoolean(PARAM_CREATE, create);
            getSupportLoaderManager().restartLoader(TAG.hashCode(), bundle, this);
        }
    }

    @Override
    public Loader<BaseRequest> onCreateLoader(int i, Bundle bundle) {
        setProgressVisibility(true);
        clearErrors();
        return new AuthLoader(this, mEmailEdit.getText().toString(), mPasswordEdit.getText().toString(), bundle.getBoolean(PARAM_CREATE));
    }

    @Override
    public void onLoadFinished(Loader<BaseRequest> loader, BaseRequest o) {
        isOperationInProgress = false;
        setProgressVisibility(false);
        if (o.haveError()) {
            clearErrors();
            if (o.getErrorCode() == FirebaseError.INVALID_PASSWORD) {
                mErrorPassword.setText(getString(o.getErrorMessageResource()));
            } else {
                mErrorEmail.setText(getString(o.getErrorMessageResource()));
            }
        } else {
            final Intent resultIntent = new Intent();
            resultIntent.putExtra(AccountManager.KEY_AUTHTOKEN, MyApi.getInstance().getToken());
            setResult(RESULT_OK, resultIntent);
            prepareFillScreen(null);
            isLoggedIn = true;
            mOverflowShadow.animate().alpha(0.7f).setDuration(450).start();
            mFillInfoParent.measure(0, 0);
            mFillInfoParent.setTranslationX(mFillInfoParent.getMeasuredWidth());
            mFillInfoParent.animate().translationX(0).setDuration(450)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            mFillInfoParent.setVisibility(View.VISIBLE);
                        }
                    }).start();
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseRequest> loader) {

    }

    private void clearErrors() {
        mErrorEmail.setError(null);
        mErrorPassword.setError(null);
    }

    private void setProgressVisibility(final boolean visible) {
        mProgressBar.animate().alpha(visible ? 1f : 0f).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                if (visible)
                    mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!visible)
                    mProgressBar.setVisibility(View.GONE);
            }
        }).start();
    }

    private static final class CleanTextWatcher implements TextWatcher {

        private ArrayList<TextView> mTextViews = new ArrayList<>();

        public CleanTextWatcher() {
        }

        public void addTextView(TextView textView) {
            mTextViews.add(textView);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            for (TextView textView : mTextViews) {
                textView.setText(null);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            for (TextView textView : mTextViews) {
                textView.setText(null);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            for (TextView textView : mTextViews) {
                textView.setText(null);
            }
        }
    }

    public static final class AuthLoader extends AsyncTaskLoader<BaseRequest> {

        private String mEmail;
        private String mPassword;
        private boolean isCreate;

        public AuthLoader(Context context, String email, String password, boolean create) {
            super(context);
            mEmail = email;
            mPassword = password;
            isCreate = create;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public BaseRequest loadInBackground() {
            return isCreate ? MyApi.getInstance().createAccount(mEmail, mPassword)
                    : MyApi.getInstance().authWithPassword(mEmail, mPassword);
        }

    }

    public static final class ImageLoader extends AsyncTaskLoader<Bitmap> {

        private CharSequence mUrl;
        private WeakReference<Bitmap> bitmapReference;

        public ImageLoader(Context context, CharSequence url) {
            super(context);
            mUrl = url;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            if (bitmapReference != null
                    && bitmapReference.get() != null
                    && !bitmapReference.get().isRecycled()) {
                deliverResult(bitmapReference.get());
            } else {
                forceLoad();
            }
        }

        @Override
        public Bitmap loadInBackground() {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(new URL(mUrl.toString()).openStream());
                bitmapReference = new WeakReference<Bitmap>(bitmap);
                return bitmapReference.get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static final class LocaleAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<Locale> mLocales = new ArrayList<>();

        public LocaleAdapter(Context context) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Locale[] locales = Locale.getAvailableLocales();
            for (int i = 0; i < locales.length; i++) {
                if (!TextUtils.isEmpty(locales[i].getDisplayCountry())
                        && !isContainsCountry(locales[i])) {
                    mLocales.add(locales[i]);
                }
            }
            Collections.sort(mLocales, new Comparator<Locale>() {
                @Override
                public int compare(Locale lhs, Locale rhs) {
                    return lhs.getDisplayCountry().compareTo(rhs.getDisplayCountry());
                }
            });
        }

        //TODO: fix for performance (current solution is too long)
        private boolean isContainsCountry(Locale locale) {
            for (Locale loc : mLocales) {
                if (loc.getCountry().equals(locale.getCountry())) {
                    return true;
                }
            }
            return false;
        }

        public int getDefault() {
            for (int i = 0; i < mLocales.size(); i++) {
                if (mLocales.get(i).equals(Locale.getDefault())) {
                    return i;
                }
            }
            return 0;
        }

        public int getDefault(String countryIso) {
            if (TextUtils.isEmpty(countryIso)) {
                return getDefault();
            }
            for (int i = 0; i < mLocales.size(); i++) {
                if (!TextUtils.isEmpty(mLocales.get(i).getCountry())
                        && mLocales.get(i).getCountry().equals(countryIso)) {
                    return i;
                }
            }
            return 0;
        }

        @Override
        public int getCount() {
            return mLocales != null ? mLocales.size() : 0;
        }

        @Override
        public Locale getItem(int position) {
            return mLocales.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.single_list_header_item, null);
                holder = new Holder();
                holder.Title = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            holder.Title.setText(mLocales.get(position).getDisplayCountry());
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.single_list_item, null);
                holder = new Holder();
                holder.Title = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            holder.Title.setText(mLocales.get(position).getDisplayCountry());
            return convertView;
        }

        class Holder {
            TextView Title;
        }

    }

}
