package maestro.my.api;

import android.accounts.*;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import maestro.my.api.request.BaseRequest;

/**
 * Created by Artyom on 9/4/2015.
 */
class Authenticator extends AbstractAccountAuthenticator {

    public static final String PARAM_ERROR = "error";
    public static final String PARAM_ERROR_CODE = "error_code";

    private Context mContext;

    public Authenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        final Intent intent = new Intent(mContext, LoginActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        final Bundle result = new Bundle();
        if (authTokenType.equals(MyApi.AUTHTOKEN_TYPE)) {
            final AccountManager manager = AccountManager.get(mContext);
            final String password = manager.getPassword(account);
            if (TextUtils.isEmpty(password)) {
                final Intent intent = new Intent(mContext, LoginActivity.class);
                intent.putExtra(AccountManager.KEY_AUTH_TOKEN_LABEL, authTokenType);
                intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
                result.putParcelable(AccountManager.KEY_INTENT, intent);
            } else {
                MyApi.getInstance().initializeIfRequired((Application) mContext.getApplicationContext());
                BaseRequest request = MyApi.getInstance().auth(password);
                if (!request.haveError()) {
                    result.putString(AccountManager.KEY_AUTHTOKEN, MyApi.getInstance().getToken());
                } else {
                    result.putString(PARAM_ERROR, mContext.getString(request.getErrorMessageResource()));
                    result.putInt(PARAM_ERROR_CODE, request.getErrorCode());
                }
            }
        } else {
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "Invalid authTokenType");
        }
        return result;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return mContext.getString(R.string.account_type);
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }

}