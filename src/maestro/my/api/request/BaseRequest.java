package maestro.my.api.request;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Artyom on 9/7/2015.
 */
public abstract class BaseRequest<ResultObject> {

    private final CountDownLatch mLatch = new CountDownLatch(1);

    private ResultObject mResult;
    private int mId;
    private int mErrorCode;
    private boolean isCanceled;
    private boolean haveError;

    public abstract void execute();

    public abstract void executeAsync();

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setResult(ResultObject result) {
        mResult = result;
    }

    public ResultObject getResult() {
        return mResult;
    }

    public void setErrorCode(int errorCode) {
        mErrorCode = errorCode;
        haveError = true;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public int getErrorMessageResource() {
        return -1;
    }

    public void cancel() {
        isCanceled = true;
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public boolean haveError() {
        return haveError;
    }

    public final void latchWait() {
        try {
            mLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public final void latchRelease() {
        mLatch.countDown();
    }

}