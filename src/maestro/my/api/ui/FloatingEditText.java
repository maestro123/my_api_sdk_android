package maestro.my.api.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.*;
import android.graphics.Paint.FontMetricsInt;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.EditText;

public class FloatingEditText extends EditText {

    public static final String TAG = FloatingEditText.class.getSimpleName();

    private enum Animation {NONE, SHRINK, GROW}

    public enum ANIMATION_TYPE {
        SLIDE, ALPHA
    }

    private Paint mFloatingHintPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private ColorStateList mDefHintColors;
    private Animation mAnimation = Animation.NONE;
    private ANIMATION_TYPE mAnimationType = ANIMATION_TYPE.SLIDE;
    private Typeface mHintTypeface;

    private int mAnimationSteps;
    private int mHintColor;
    private int mAnimationFrame;

    private float mHintScale;

    private boolean mWasEmpty;

    public FloatingEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FloatingEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FloatingEditText(Context context) {
        super(context);
        init();
    }

    public void init() {
        mHintScale = 12f / 18f;
        mAnimationSteps = mAnimationType == ANIMATION_TYPE.ALPHA ? 25 : 6;
        mDefHintColors = getHintTextColors();
        TypedValue value = new TypedValue();
        getContext().getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.colorPrimary, value, true);
        mHintColor = value.data;
        mWasEmpty = TextUtils.isEmpty(getText());
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }

    public void setHintTypeface(Typeface typeface) {
        mHintTypeface = typeface;
    }

    @Override
    public int getCompoundPaddingTop() {
        mFloatingHintPaint.setTextSize(getTextSize() * mHintScale);
        final FontMetricsInt metrics = mFloatingHintPaint.getFontMetricsInt();
        final int floatingHintHeight = (int) ((metrics.bottom - metrics.top) * mHintScale);
        return super.getCompoundPaddingTop() + floatingHintHeight;
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        final boolean isEmpty = TextUtils.isEmpty(getText());
        if (mWasEmpty == isEmpty) {
            return;
        }
        mWasEmpty = isEmpty;
        if (!isShown()) {
            return;
        }
        if (isEmpty) {
            mAnimation = Animation.GROW;
            setHintTextColor(Color.TRANSPARENT);
        } else {
            mAnimation = Animation.SHRINK;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (TextUtils.isEmpty(getHint())) {
            return;
        }

        final boolean isAnimating = mAnimation != Animation.NONE;

        if (!isAnimating && TextUtils.isEmpty(getText())) {
            return;
        }

        mFloatingHintPaint.set(getPaint());
        mFloatingHintPaint.setTypeface(mHintTypeface);

        final float normalHintSize = getTextSize();
        final float floatingHintSize = normalHintSize * mHintScale;
        mFloatingHintPaint.setTextSize(floatingHintSize);
        final Rect bounds = new Rect();
        mFloatingHintPaint.getTextBounds(String.valueOf(getHint()), 0, getHint().length(), bounds);

        final float hintPosX = getCompoundPaddingLeft() + getScrollX();
        final float normalHintPosY = getBaseline();

        final float floatingHintPosY = bounds.bottom - bounds.top;//getTop();//normalHintPosY + getPaint().getFontMetricsInt().top + getScrollY();
        final float floatStep = mAnimation == Animation.GROW ? 1f / mAnimationSteps * mAnimationFrame : 1f / mAnimationSteps * (mAnimationSteps - mAnimationFrame);

        if (!isAnimating) {
            mFloatingHintPaint.setColor(mHintColor);
            mFloatingHintPaint.setTextSize(floatingHintSize);
            canvas.drawText(getHint().toString(), hintPosX, floatingHintPosY, mFloatingHintPaint);
            return;
        }

        if (mAnimationType == ANIMATION_TYPE.SLIDE) {
            mFloatingHintPaint.setColor(blendColors(mDefHintColors.getColorForState(getDrawableState(),
                    mDefHintColors.getDefaultColor()), mHintColor, floatStep));
            if (mAnimation == Animation.SHRINK) {
                drawAnimationFrame(canvas, normalHintSize, floatingHintSize,
                        hintPosX, normalHintPosY, floatingHintPosY);
            } else {
                drawAnimationFrame(canvas, floatingHintSize, normalHintSize,
                        hintPosX, floatingHintPosY, normalHintPosY);
            }
        } else {
            mFloatingHintPaint.setColor(mHintColor);
            mFloatingHintPaint.setAlpha((int) (255 * (1f - floatStep)));
            mFloatingHintPaint.setTextSize(floatingHintSize);
            canvas.drawText(getHint().toString(), hintPosX, floatingHintPosY, mFloatingHintPaint);
            if (mAnimation == Animation.GROW) {
                setHintTextColor(mDefHintColors.getDefaultColor());
            }
        }

        mAnimationFrame++;

        if (mAnimationFrame == mAnimationSteps) {
            if (mAnimation == Animation.GROW) {
                setHintTextColor(mDefHintColors.getDefaultColor());
            }
            mAnimation = Animation.NONE;
            mAnimationFrame = 0;
        }

        invalidate();
    }

    private int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    private void drawAnimationFrame(Canvas canvas, float fromSize, float toSize, float hintPosX, float fromY, float toY) {
        final float textSize = lerp(fromSize, toSize);
        final float hintPosY = lerp(fromY, toY);
        mFloatingHintPaint.setTextSize(textSize);
        canvas.drawText(getHint().toString(), hintPosX, hintPosY, mFloatingHintPaint);
    }

    private float lerp(float from, float to) {
        final float alpha = (float) mAnimationFrame / (mAnimationSteps - 1);
        return from * (1 - alpha) + to * alpha;
    }
}
