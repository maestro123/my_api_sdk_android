package maestro.my.api;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import com.firebase.client.*;
import maestro.my.api.model.User;
import maestro.my.api.request.BaseRequest;

/**
 * Created by Artyom on 9/4/2015.
 */
public class MyApi {

    public static final String TAG = MyApi.class.getSimpleName();

    private static volatile MyApi instance;

    public static synchronized MyApi getInstance() {
        return instance != null ? instance : (instance = new MyApi());
    }

    MyApi() {
    }

    public static final String ACCOUNT_TYPE = "maestro.my.account";
    public static final String AUTHTOKEN_TYPE = "maestro.my.token";

    private static final String CHILD_USER_INFO = "user_info";
    private static final String CHILD_FIRST_NAME = "first_name";
    private static final String CHILD_LAST_NAME = "last_name";
    private static final String CHILD_GENDER = "gender";
    private static final String CHILD_BIRTH_DATE = "birth_date";
    private static final String CHILD_COUNTRY = "country";

    private static final Object mLock = new Object();

    public enum UserState {
        Idle, LoginInProgress, LoggedIn
    }

    private Application mApplication;
    private Firebase mUserBase;
    private Firebase mAppBase;
    private User mUser;
    private String mToken;
    private AccountManager mAccountManager;
    private UserState mUserState = UserState.Idle;

    public void initialize(Application application, String appBase) {
        mApplication = application;
        mAccountManager = AccountManager.get(application);
        Firebase.setAndroidContext(application);
        mUserBase = new Firebase("https://my0.firebaseio.com/");
        if (!TextUtils.isEmpty(appBase)) {
            mAppBase = new Firebase(appBase);
        }
    }

    public void initializeIfRequired(Application application) {
        if (!isInitialized()) {
            initialize(application, null);
        }
    }

    public boolean isInitialized() {
        return mUserBase != null;
    }

    public void setAppBase(String appBase) {
        mAppBase = TextUtils.isEmpty(appBase) ? null : new Firebase(appBase);
    }

    /**
     * Auth logic
     */

    BaseRequest createAccount(final String name, final String password) {
        synchronized (mLock) {
            return processCreateAccount(name, password);
        }
    }

    BaseRequest processCreateAccount(final String name, final String password) {
        BaseRequest request = new CreateAccountRequest(name, password);
        request.execute();
        if (!request.haveError()) {
            return processAuthWithPassword(name, password);
        }
        return request;
    }

    BaseRequest authWithPassword(String account, String password) {
        synchronized (mLock) {
            return processAuthWithPassword(account, password);
        }
    }

    BaseRequest processAuthWithPassword(final String account, final String password) {
        mUserState = UserState.LoginInProgress;
        AuthWithPasswordRequest request = new AuthWithPasswordRequest(account, password);
        request.execute();
        afterAuth(request, request.getResult());
        return request;
    }

    BaseRequest auth(String token) {
        synchronized (mLock) {
            return processAuth(token);
        }
    }

    BaseRequest processAuth(String token) {
        AuthAccountRequest request = new AuthAccountRequest(token);
        request.execute();
        afterAuth(request, request.getResult());
        return request;
    }

    private final void afterAuth(BaseRequest request, AuthData data) {
        if (!request.haveError()) {
            mToken = data.getToken();
            ensureUserData(data);
            mUserState = UserState.LoggedIn;
        } else {
            mUserState = UserState.Idle;
        }
    }

    private final void ensureUserData(final AuthData data) {
        mUser = new User();
        mUser.setId(data.getUid());
        GetUserInfoRequest request = new GetUserInfoRequest();
        request.execute();
        //TODO: process any error?
    }

    public final void updateUserData(User user) {
        Firebase base = mUserBase.child(user.getId()).child(CHILD_USER_INFO);
        base.child(CHILD_FIRST_NAME).setValue(user.getFirstName());
        base.child(CHILD_LAST_NAME).setValue(user.getLastName());
        base.child(CHILD_GENDER).setValue(user.getGender());
        base.child(CHILD_BIRTH_DATE).setValue(user.getBirthDate());
        base.child(CHILD_COUNTRY).setValue(user.getCountryIso());
    }

    void addAccountToSystem(String account, String token, Bundle userInfo) {
        mAccountManager.addAccountExplicitly(new Account(account, ACCOUNT_TYPE), token, userInfo);
    }

    public boolean isAccountExistsInSystem(String email) {
        return getSystemAccount(email) != null;
    }

    public Account getSystemAccount(String email) {
        final Account[] ac = mAccountManager.getAccountsByType(ACCOUNT_TYPE);
        if (ac != null && ac.length != 0) {
            for (Account _a : ac) {
                if (_a.name.equalsIgnoreCase(email)) {
                    return _a;
                }
            }
        }
        return null;
    }

    public Firebase getUserBase() {
        return mUserBase;
    }

    public Firebase getAppBase() {
        return mAppBase;
    }

    public String getToken() {
        return mToken;
    }

    public User getUser() {
        return mUser;
    }

    public static final int getErrorMessage(int errorCode) {
        //TODO: implement
        return -1;
    }

    private static abstract class BaseAuthApiRequest<ResultObject> extends BaseRequest<ResultObject> {

        @Override
        public int getErrorMessageResource() {
            switch (getErrorCode()) {
                case 100:
                    return R.string.err_authentication_in_progress;
                case FirebaseError.EMAIL_TAKEN:
                    return R.string.err_email_already_used;
                case FirebaseError.NETWORK_ERROR:
                    return R.string.err_network;
                case FirebaseError.INVALID_EMAIL:
                    return R.string.err_invalid_email;
                case FirebaseError.INVALID_PASSWORD:
                    return R.string.err_invalid_password;
                case FirebaseError.USER_DOES_NOT_EXIST:
                    return R.string.err_user_does_not_exists;
                default:
                    return R.string.err_unknown;
            }
        }
    }

    private static final class CreateAccountRequest extends BaseAuthApiRequest {

        private String mAccount;
        private String mPassword;
        private MyApi myApi;

        CreateAccountRequest(String account, String password) {
            myApi = MyApi.getInstance();
            mAccount = account;
            mPassword = password;
        }

        @Override
        public void execute() {
            myApi.mUserBase.createUser(mAccount, mPassword, new Firebase.ResultHandler() {
                @Override
                public void onSuccess() {
                    myApi.addAccountToSystem(mAccount, mPassword, null);
                    latchRelease();
                }

                @Override
                public void onError(FirebaseError firebaseError) {
                    setErrorCode(firebaseError.getCode());
                    latchRelease();
                }
            });
            latchWait();
        }

        @Override
        public void executeAsync() {
            throw new UnsupportedOperationException("Async not available");
        }
    }

    private static final class AuthWithPasswordRequest extends BaseAuthApiRequest<AuthData> {

        private String mAccount;
        private String mPassword;
        private MyApi myApi;

        AuthWithPasswordRequest(String account, String password) {
            myApi = MyApi.getInstance();
            mAccount = account;
            mPassword = password;
        }

        @Override
        public void execute() {
            myApi.mUserBase.authWithPassword(mAccount, mPassword, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    Account systemAccount = myApi.getSystemAccount(mAccount);
                    String token = authData.getToken();
                    if (systemAccount == null) {
                        myApi.addAccountToSystem(mAccount, token, null);
                    } else {
                        myApi.mAccountManager.setPassword(systemAccount, token);
                    }
                    setResult(authData);
                    latchRelease();
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    setErrorCode(firebaseError.getCode());
                    latchRelease();
                }
            });
            latchWait();
        }

        @Override
        public void executeAsync() {
            throw new UnsupportedOperationException("Async not available");
        }
    }

    private static final class AuthAccountRequest extends BaseAuthApiRequest<AuthData> {

        private String mToken;
        private MyApi myApi;

        AuthAccountRequest(String token) {
            myApi = MyApi.getInstance();
            mToken = token;
        }

        @Override
        public void execute() {
            myApi.mUserBase.authWithCustomToken(mToken, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    //TODO: update account manager
                    setResult(authData);
                    latchRelease();
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    latchRelease();
                }
            });
            latchWait();
        }

        @Override
        public void executeAsync() {
            throw new UnsupportedOperationException("Async not available");
        }
    }

    private static final class GetUserInfoRequest extends BaseAuthApiRequest {

        private MyApi myApi;

        public GetUserInfoRequest() {
            myApi = MyApi.getInstance();
        }

        @Override
        public void execute() {
            //TODO: add email set logic from data provider
            myApi.mUserBase.child(myApi.mUser.getId()).child(CHILD_USER_INFO).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(CHILD_FIRST_NAME)) {
                        myApi.mUser.setFirstName((String) dataSnapshot.child(CHILD_FIRST_NAME).getValue());
                    }
                    if (dataSnapshot.hasChild(CHILD_LAST_NAME)) {
                        myApi.mUser.setLastName((String) dataSnapshot.child(CHILD_LAST_NAME).getValue());
                    }
                    if (dataSnapshot.hasChild(CHILD_BIRTH_DATE)) {
                        myApi.mUser.setBirthDate((Long) dataSnapshot.child(CHILD_BIRTH_DATE).getValue());
                    }
                    if (dataSnapshot.hasChild(CHILD_GENDER)) {
                        myApi.mUser.setGender((Long) dataSnapshot.child(CHILD_GENDER).getValue());
                    }
                    if (dataSnapshot.hasChild(CHILD_COUNTRY)) {
                        myApi.mUser.setCountryIso((String) dataSnapshot.child(CHILD_COUNTRY).getValue());
                    }
                    latchRelease();
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    latchRelease();
                }
            });
            latchWait();
        }

        @Override
        public void executeAsync() {

        }
    }

}
